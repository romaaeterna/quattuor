Quattuor
========


Introduction
------------

Quattuor is an educational game that combines vocabulary learning with the famous game Connect Four.

You can find detailed information including game instructions in the `online documentation <https://quattuor.readthedocs.io/en/latest/>`_.

Quattuor is hosted on `GitLab <https://gitlab.com/romaaeterna/quattuor>`_.


License
-------

This program is released under the `GNU General Public License (GPL) version 3 <http://www.gnu.org/licenses/>`_.

See the file `COPYING <COPYING>`_ for more information.


Installation
------------

Meson installation
~~~~~~~~~~~~~~~~~~

To install Quattuor simply run the script ``install.sh`` in this directory.

Running uninstalled
~~~~~~~~~~~~~~~~~~~

If you don't want to install Quattuor, simply execute ``run.sh`` in this directory.


Runtime Dependencies
--------------------

Quattuor strongly depends on Gtk+3 and Python 3:

- Gtk+ 3 (>= 3.16)
- Python 3 (>= 3.3)
- PyGObject (>= 3.16)
- Pycairo (>= 1.10)


Special thanks to
-----------------

- Joe Hamilton (for his fabulous `simpleaudio package <https://github.com/hamiltron/py-simple-audio>`_)
- the Latin students of the `Vicco-von-Bülow-Gymnasium Falkensee <http://www.vicco-von-buelow-gymnasium-falkensee.de/>`_ (for being enthusiastic beta testers)
