Credits
=======


Quattuor is developed by Thomas Dähnrich.

Special thanks go to:

- Joe Hamilton (for his fabulous `simpleaudio package <https://github.com/hamiltron/py-simple-audio>`_)
- the developers of `MSYS2 <https://www.msys2.org/>`_, `PyInstaller <https://www.pyinstaller.org/>`_ and `Inno Setup <http://www.jrsoftware.org/isinfo.php>`_
- the Latin students of the `Vicco-von-Bülow-Gymnasium Falkensee <http://www.vicco-von-buelow-gymnasium-falkensee.de/>`_ (for being enthusiastic beta testers)
