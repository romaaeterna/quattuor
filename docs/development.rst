Development
===========


Roadmap
-------

The current version of Quattuor is 1.1.0.

For future releases the following features are planned:

- better support for HiDPI
- port to GTK 4


Bugs
----

Known Bugs
~~~~~~~~~~

At the moment no bugs are known.

Filing A Bug
~~~~~~~~~~~~

If you’ve found a bug in Quattuor, please head over to GitLab and `file a report <https://gitlab.com/romaaeterna/quattuor/issues>`_.
Filing bugs helps improve the software for everyone.


Source Code
-----------

The full source code of Quattuor is hosted on `GitLab <https://gitlab.com/romaaeterna/quattuor>`_.
