Getting started
===============


Vocabulary list
---------------

To play Quattuor you need a vocabulary list.
You can use the build-in vocabulary editor, which features easy and intuitive operation, to create new lists or edit existing ones.

.. image:: images/screen_editor.png
   :width: 600px

Alternatively, you can create and edit vocabulary lists as CSV or TXT files by your own.
Please note that the application can only load files with the following data structure::

   [unit],[vocable],[translation]

Your vocabulary list should look like this example (separated by comma, semicolon or tab)::

   1,this,dies
   1,is,ist
   2,an,ein
   2,example,Beispiel


Settings
--------

There are several general settings you may adapt to your needs.

.. image:: images/screen_settings1.png
   :width: 425px

Moreover you can change some options regarding to vocabulary lists,
especially the mode of vocabulary selection during the game
(only source language / only target language / both languages).

.. image:: images/screen_settings2.png
   :width: 425px


Teams and player numbers
------------------------

Quattuor has got a game mode for playing in two competitive teams.
Both teams should be equally big and consist of 4 to 16 players.
Each team player has to be allocated a number.
Players with the same number compete against each other.
When every student is assigned to his/her team and number, you can start the game.
