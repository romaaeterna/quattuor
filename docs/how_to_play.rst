How to play
===========


Setting up the game
-------------------

After starting the game you'll see the main window of the application.
Open the options window by clicking on the button "New Game".

.. image:: images/screen_mainwindow.png
   :width: 500px

First select one of the following three game modes:

- Team mode (for teams with 4 to 16 players, with vocabulary learning)
- Duel mode (for two players, with vocabulary learning)
- Classic mode (for two players, without vocabulary learning)

If you have selected a game mode with vocabulary learning, click on the button "Load Vocabulary" and navigate to the file you want to open.
Then choose the units you want your students to repeat.

.. image:: images/screen_newgame1.png
   :width: 500px

Type in the name of the players/teams and select the number of players if you play the game in team mode. Finally click on the button "Apply".

.. image:: images/screen_newgame2.png
   :width: 500px


Playing the game
----------------

The objective of the game is to be the first to form a horizontal, vertical or diagonal line of four disks of the own color.

Team mode and duel mode
~~~~~~~~~~~~~~~~~~~~~~~

The game control is on the left side of the window. There are buttons for the player number (only in team mode), the vocable to be guessed and its correct translation. The buttons must be clicked one after the other.

.. image:: images/screen_gamecontrol.png

Now you'll see a yellow button, a red button and a button with the letter "X". Click on the yellow button, if the first player/team won this round, click on the red button, if the second player/team won this round and click on the "X" button, if there is no winner because both players/teams did not know the vocable or translated it incorrectly.

.. image:: images/screen_winner.png

The player/team who guessed the word can drop one disk into the game board. Simply click on the arrow to choose the position.

.. image:: images/screen_position.png

After that the next round starts. If you play in team mode, you can remove or change the color of a disk by clicking on it.

.. image:: images/screen_disk.png

If one player/team won the game, a victory message will appear on the screen.

.. image:: images/screen_victory.png

Classic mode
~~~~~~~~~~~~

In classic mode the game is played by two players alternately. Click on the arrow to choose the column you want your disk to fall down into. That's it!
