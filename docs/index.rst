Quattuor
========

.. image:: images/logo.png
    :align: center

Quattuor is an educational game that combines vocabulary learning with the famous game Connect Four.

The program is released under the `GNU General Public License (GPL) version 3 <http://www.gnu.org/licenses/>`_.


Contents
--------

.. toctree::
   :maxdepth: 2

   installation_linux
   installation_windows
   getting_started
   how_to_play
   development
   credits
