Installation on Linux
=====================


Installing Quattuor
------------------

Before installing Quattuor please check the `Dependencies`_ at the bottom.

After downloading the `source code from GitLab <https://gitlab.com/romaaeterna/quattuor/-/archive/master/quattuor-master.zip>`_ extract the archive to a folder of your choice.

To install Quattuor simply run the script ``install.sh`` in the main directory.

To uninstall the application run the script ``uninstall.sh``.


Running uninstalled
-------------------

If you downloaded the application from GitLab and don't want to install Quattuor, simply execute ``run.sh`` in the main directory.

In this case the game is only available with an English graphical user interface.


Dependencies
------------

Build-time dependencies
~~~~~~~~~~~~~~~~~~~~~~~

As build-time dependencies you need `Meson <http://mesonbuild.com/>`_ and `gettext <https://www.gnu.org/software/gettext/>`_ for localization.

Install the dependenices by using the following shell command:

- Arch Linux: ``sudo pacman -S meson gettext``
- Debian/Ubuntu: ``sudo apt install meson gettext``
- Fedora: ``sudo dnf install meson gettext``
- openSUSE: ``sudo zypper install meson gettext-tools``

Runtime dependencies
~~~~~~~~~~~~~~~~~~~~

Quattuor strongly depends on Python 3 and GTK+ 3:

- GTK+ 3 (>= 3.16)
- Python 3 (>= 3.3)
- PyGObject (>= 3.16)
- Pycairo (>= 1.10)

Install the runtime dependencies by using the following shell command:

- Arch Linux: ``sudo pacman -S python-cairo python-gobject gtk3``
- Debian/Ubuntu: ``sudo apt install python3-gi-cairo gir1.2-gtk-3.0``
- Fedora: ``sudo dnf install pycairo python3-gobject3 gtk3``
- openSUSE: ``sudo zypper install python3-cairo python3-gobject-Gdk libgtk-3-0``
