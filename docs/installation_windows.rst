Installation on Windows
=======================


Installing Quattuor
-------------------

To install Quattuor just follow the setup wizard of `QuattuorSetup.exe <https://romaaeterna.de/files/QuattuorSetup.exe>`_.


Building
--------

You can build a Windows executable file and package on your own. Therefor you need to install the development platform `MSYS2 <https://www.msys2.org/>`_. Just follow the installation instructions given in the `documentaton of PyGObject <https://pygobject.readthedocs.io/en/latest/getting_started.html#windows-getting-started>`_.

For freezing the Python files into a standalone executable you have to use `PyInstaller <https://www.pyinstaller.org/>`_ (>= 3.6). To start the build process open a mingw64 terminal and type in the following command in the main directory of Quattuor::

   pyinstaller setup-win.spec

The full package will be created in the folder ``dist``.
