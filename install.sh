#!/bin/bash
if find ./ -path "*uninstall.bak*" | grep -q .
then
    echo "Quattuor is already installed."
    echo "You can uninstall it via './uninstall.sh'."
else
    meson build
    ninja -C build
    sudo ninja -C build install
fi
