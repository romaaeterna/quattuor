#!/usr/bin/env python3

import os
import subprocess
import sysconfig

BUILD_DIR = os.environ.get('MESON_BUILD_ROOT', os.getcwd())
DEST_DIR = os.environ.get('DESTDIR', '')
PREFIX = os.environ.get('MESON_INSTALL_DESTDIR_PREFIX', os.path.join(DEST_DIR, 'usr', 'local'))
SOURCE_DIR = os.environ.get('MESON_SOURCE_ROOT', os.path.join(os.getcwd(), '..'))

BIN_DIR = os.path.join(PREFIX, 'bin')
BIN_FILE = os.path.join(BIN_DIR, 'quattuor')
DATA_DIR = os.path.join(PREFIX, 'share')
ICONCACHE_DIR = os.path.join(DATA_DIR, 'icons', 'hicolor')
LOCALE_DIR = os.path.join(DATA_DIR, 'locale')
PYTHON_DIR = os.path.join(sysconfig.get_path('purelib', vars={'base': PREFIX}), 'quattuor')
PYTHON_FILE = os.path.join(PYTHON_DIR, 'quattuor.py')
UNINSTALL_FILE = os.path.join(SOURCE_DIR, 'uninstall.sh')
UNINSTALL_BACKUP = os.path.join(BUILD_DIR, 'uninstall.bak')

# create symlink in 'bin' to 'PYTHON_DIR'
if not os.path.exists(BIN_DIR):
    os.makedirs(BIN_DIR)
subprocess.call(['ln', '-sf', PYTHON_FILE, BIN_FILE])

# create icon cache
if not DEST_DIR:
    if not os.path.exists(ICONCACHE_DIR):
        os.makedirs(ICONCACHE_DIR)
    subprocess.call(['gtk-update-icon-cache', '-tf', ICONCACHE_DIR])

# add files for uninstallation to 'uninstall.sh'
if PREFIX != '/app':
    subprocess.call(['cp', UNINSTALL_FILE, UNINSTALL_BACKUP])
    os.chmod(UNINSTALL_BACKUP, 0o664)
    with open(UNINSTALL_FILE, 'a') as f:
        f.write('sudo rm {}'.format(BIN_FILE))
        f.write('\n')
        for root, dirs, files in os.walk(LOCALE_DIR):
            if 'quattuor.mo' in files:
                f.write('sudo rm {}'.format(os.path.join(root, 'quattuor.mo')))
                f.write('\n')
        # update application cache in KDE
        # NOTE: os.environ.get('XDG_CURRENT_DESKTOP') is 'Null' in Meson
        if 'kbuildsycoca5' in os.listdir('/usr/bin'):
            f.write('kbuildsycoca5')
            f.write('\n')
        f.write('cp {} {}'.format(UNINSTALL_BACKUP, UNINSTALL_FILE))
        f.write(' && sudo rm {}'.format(UNINSTALL_BACKUP))
