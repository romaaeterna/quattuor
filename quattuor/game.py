#!/usr/bin/env python3
#
# This file is part of Quattuor.
#
# Copyright (C) 2020-2021 - Thomas Dähnrich <develop@tdaehnrich.de>
#
# Quattuor is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Quattuor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quattuor. If not, see <http://www.gnu.org/licenses/>.

import gi
import os
import random
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gdk
from gi.repository import GLib
from gi.repository import GObject
from gi.repository import Gtk

from settings import APP_ID, PIXMAPS_DIR, SOUNDS_DIR, TEAMS_FILE, UI_DIR, VOC_LISTS_FILE
from settings import get_scale_factor, get_settings, load_css_provider, setup_language


# get settings and setup language

get_settings()
_ = setup_language()


# create vocable cards and teams

class Disk(object):

    def __init__(self, button, color, dropped, x, y):

        self.button = button
        self.color = color
        self.dropped = dropped
        self.x = x
        self.y = y


class Team(object):

    def __init__(self, name, color):

        self.name = name
        self.color = color
        self.active = False


# main functions of game

def share_widgets_with_settings(self):

    global boxGame, boxGameControl, gridGameGrid, position_buttons, winGame

    boxGame = self.boxGame
    boxGameControl = self.boxGameControl
    gridGameGrid = self.gridGameGrid
    position_buttons = (self.btnPosition0, self.btnPosition1, self.btnPosition2,
        self.btnPosition3, self.btnPosition4, self.btnPosition5, self.btnPosition6)
    winGame = self.winGame


def setup_theme_and_headerbar(self):

    from settings import dark_theme

    Gtk.Settings.get_default().set_property("gtk-application-prefer-dark-theme", dark_theme)

    decoration = Gtk.Settings.get_default().get_property("gtk-decoration-layout")
    if "close" in decoration.split(":")[0]:
        self.headGame.child_set_property(self.menbtnGame, "pack-type", Gtk.PackType.START)
        self.headGame.child_set_property(self.btnNewGame, "pack-type", Gtk.PackType.END)


def setup_control_layout(self):

    from settings import position_arrows, position_control 
    
    if position_control == 1:
        self.boxGame.child_set_property(self.boxGameControl, "pack-type", Gtk.PackType.END)
    if position_arrows == 1:
        for i in range(len(position_buttons)):
            self.gridGameGrid.remove(position_buttons[i])
            self.gridGameGrid.attach(position_buttons[i], i, 7, 1, 1)


def harmonize_size_of_options_window(self):

    options_height = self.gridTeams.get_preferred_height()
    self.boxDuelMode.set_property("height-request", options_height[0])
    self.boxClassicMode.set_property("height-request", options_height[0])

    options_width = self.lblPlayerNo.get_preferred_width()[0] + \
        self.cmbPlayerNo.get_preferred_width()[0]
    self.gridTeams.set_property("width-request", options_width)


def build_disks(self):

    global disks

    disks = []
    for y in range(1,7):
        for x in range(7):
            btnDisk = Gtk.Button.new()
            btnDisk.set_property("sensitive", False)
            btnDisk.set_property("visible", True)
            btnDisk.set_property("vexpand", True)
            btnDisk.set_property("height-request", 120)
            btnDisk.set_property("width-request", 120)
            btnDisk.get_style_context().add_class("buttonDisk")
            btnDisk.connect("clicked", self.on_btnDisk_clicked)
            disks.append(Disk(btnDisk, 0, False, x, 6-y))
            self.gridGameGrid.attach(btnDisk, x, y, 1, 1)


def get_voc_lists():

    global voc_list_current, voc_lists

    saved_voc_lists = []
    voc_list_current = {}
    voc_lists = []

    if os.path.exists(VOC_LISTS_FILE):
        with open(VOC_LISTS_FILE, 'r', encoding='utf-8') as f:
            saved_voc_lists = list(filter(None, f.read().split("\n")))

    for voc_list in saved_voc_lists:
        if os.path.exists(voc_list):
            voc_lists.append(voc_list)


def show_voc_lists(self):

    global voc_lists

    for i in range(len(voc_lists), -1):
        if not os.path.exists(voc_lists[i]):
            voc_lists.pop(i-1)

    if len(voc_lists) > 4:
        voc_lists.pop(-1)

    self.btnVocabularyRecent.show()
    menus = (self.menuRecent1, self.menuRecent2, self.menuRecent3, self.menuRecent4)
    if not len(voc_lists):
        self.btnVocabularyRecent.hide()
        for menu in menus:
            menu.hide()
    if len(voc_lists) >= 1:
        self.menuRecent1.set_property("text", os.path.basename(voc_lists[0]))
        self.menuRecent1.show()
        for menu in menus[1:4]:
            menu.hide()
    if len(voc_lists) >= 2:
        self.menuRecent2.set_property("text", os.path.basename(voc_lists[1]))
        self.menuRecent2.show()
        for menu in menus[2:4]:
            menu.hide()
    if len(voc_lists) >= 3:
        self.menuRecent3.set_property("text", os.path.basename(voc_lists[2]))
        self.menuRecent3.show()
        for menu in menus[3:4]:
            menu.hide()
    if len(voc_lists) == 4:
        self.menuRecent4.set_property("text", os.path.basename(voc_lists[3]))
        self.menuRecent4.show()


def save_voc_lists():

    with open(VOC_LISTS_FILE, 'w', encoding='utf-8') as f:
        for voc_list in voc_lists:
            f.write(voc_list + "\n")


def get_team_names():

    global team_names

    team_names = []
    if os.path.exists(TEAMS_FILE):
        with open(TEAMS_FILE, 'r', encoding='utf-8') as f:
            team_names = list(filter(None, f.read().split("\n")))


def show_team_names(self):

    self.listTeamA.clear()
    self.listTeamB.clear()
    for team_name in team_names:
        self.listTeamA.append([team_name])
        self.listTeamB.append([team_name])


def save_team_names():

    global team_names

    team_names.sort()
    with open(TEAMS_FILE, 'w', encoding='utf-8') as f:
        for team_name in team_names:
            f.write(team_name + "\n")


def load_vocabulary_list(self, file_path):

    from list import get_vocabulary, get_vocabulary_error
    global voc_file_path, voc_units

    secondary_text, voc_units = get_vocabulary(self.listUnits, file_path)
    if secondary_text:
        self.lblListName.set_text(_("No list loaded."))
        change_voc_buttons_sensitivity(self, False)
        get_vocabulary_error(file_path, self.winGame, secondary_text)
    else:
        voc_file_path = file_path
        self.lblListName.set_text(os.path.basename(voc_file_path))
        change_voc_buttons_sensitivity(self, True)
        if voc_list_current:
            for unit in voc_list_current["units_active"]:
                for i in range(len(self.listUnits)):
                    if unit == self.listUnits[i][1]:
                        self.listUnits[i][0] = True


def change_voc_buttons_sensitivity(self, boolean):

    for widget in (self.btnListSelectAll, self.btnListDeselectAll, self.btnListTop,
        self.btnListUp, self.btnListDown, self.btnListBottom):
        widget.set_sensitive(boolean)


def create_teams(self):

    global teamA, teamB, team_names

    nameA = self.entTeamA.get_text()
    nameB = self.entTeamB.get_text()

    if not nameA or not nameB or nameA == nameB:
        message_text = _("Please type in two different names!")
        show_warning_dialog(self.winGame, message_text)
        return False

    teamA = Team(nameA, Gdk.RGBA(255, 215, 0, 1))
    teamB = Team(nameB, Gdk.RGBA(153, 0, 0, 1))

    if not nameA in team_names:
        team_names.append(nameA)
    if not nameB in team_names:
        team_names.append(nameB)

    return True


def choose_unit(self):

    message_text = _("You have to choose one unit at least.")
    for i in range(len(self.listUnits)):
        if self.listUnits[i][0]:
            message_text = ""
            break
    if message_text:
        show_warning_dialog(self.winGame, message_text)
        return False

    return True


def set_player_number(self, duel_mode):

    global player_number, players

    if not duel_mode:
        player_number = self.cmbPlayerNo.get_active()+4
        self.btnPlayer.show()
        self.lblTopPlayer.show()
        self.btnVocable.set_sensitive(False)
        self.lblVocable.set_text("")
    else:
        player_number = 2
        self.btnPlayer.hide()
        self.lblTopPlayer.hide()
        self.btnVocable.set_sensitive(True)
        self.lblVocable.set_text("?")
    players = []


def show_warning_dialog(parent_window, message_text):

    dialog = Gtk.MessageDialog(parent_window, 0, Gtk.MessageType.WARNING,
        Gtk.ButtonsType.OK, message_text)
    dialog.run()
    dialog.destroy()


def show_game_options(self):

    from settings import default_file

    show_team_names(self)
    show_voc_lists(self)

    voc_file_path = ''
    if voc_list_current:
        load_vocabulary_list(self, voc_list_current["path"])
    elif os.path.exists(default_file):
        load_vocabulary_list(self, default_file)

    if not Gtk.check_version(3,22,0):
        self.popOptions.popup()
    else:
        self.popOptions.show_all()


def reset_game(self, full_reset):

    for disk in disks:
        disk.button.set_sensitive(False)
        if full_reset:
            disk.button.get_style_context().remove_class("colorA")
            disk.button.get_style_context().remove_class("colorB")
        disk.color = 0
        disk.dropped = False

    if full_reset:
        self.boxGameControl.hide()
        self.btnPlayer.set_label("?")
        self.lblTranslation.set_text("")
        for button in self.gridWinner.get_children():
            button.set_label("")
        self.btnTeamA.get_style_context().remove_class("colorA")
        self.btnTeamB.get_style_context().remove_class("colorB")
        change_position_buttons_visibility(False)
    else:
        for widget in self.boxGameControl.get_children():
            if type(widget) == Gtk.Button:
                widget.set_sensitive(False)
        for button in position_buttons:
            button.set_sensitive(False)
        for button in self.gridWinner.get_children():
            button.set_sensitive(False)

    self.btnNewGame.show()
    if not self.winGame.is_maximized():
        self.winGame.resize(1, 1)


def setup_game(self, classic_mode):

    global voc_active, voc_list_current, voc_lists

    if not classic_mode:
        units_active = []
        voc_active = []
        for i in range(len(self.listUnits)):
            if self.listUnits[i][0]:
                for unit in voc_units:
                    if unit == self.listUnits[i][2]:
                        units_active.append(self.listUnits[i][1])
                        voc_active.extend(voc_units[unit])
        voc_list_current = {"path": voc_file_path, "units_active": units_active}
        if voc_list_current["path"] in voc_lists:
            voc_lists.remove(voc_list_current["path"])
        voc_lists.insert(0, voc_list_current["path"])

    play_sound('start.wav')
    change_position_buttons_visibility(True)
    header_height = self.headGame.get_preferred_height()
    self.headGame.set_property("height-request", header_height[0])
    self.btnNewGame.hide()
    self.btnPlayer.set_sensitive(True)
    for button in position_buttons:
        button.set_use_underline(False)
    change_position_buttons_sensitivity(classic_mode)
    color_position_buttons(classic_mode, False)
    self.boxGameControl.set_visible(not classic_mode)
    if not self.winGame.is_maximized():
        self.winGame.resize(1, 1)


def select_vocable(self):

    from settings import selection

    global translation

    if self.lblVocable.get_text() == "?":
        return False

    count = int(self.lblVocable.get_text())
    count -= 1
    self.lblVocable.set_text(str(count))
    if count == 0:
        voc_current = random.randint(0, len(voc_active)-1)
        if selection == "source":
            vocable = voc_active[voc_current][0]
            translation = voc_active[voc_current][1]
        elif selection == "target":
            vocable = voc_active[voc_current][1]
            translation = voc_active[voc_current][0]
        else:
            number = random.randint(0, 1)
            vocable = voc_active[voc_current][number]
            if number == 0:
                translation = voc_active[voc_current][1]
            else:
                translation = voc_active[voc_current][0]
        self.lblVocable.set_text(vocable)
        self.btnTranslation.set_sensitive(True)
        self.lblTranslation.set_text("?")
        return False
    return True


def change_position_buttons_sensitivity(boolean):

    for button in position_buttons:
        # the underline property is a hack to make the button permanently insensitive
        if not button.get_use_underline():
            button.set_sensitive(boolean)


def change_position_buttons_visibility(boolean):

    for button in position_buttons:
        button.set_visible(boolean)


def color_position_buttons(booleanA, booleanB):

    from settings import position_arrows

    teamA.active = booleanA
    teamB.active = booleanB

    if position_arrows == 0:
        arrow = "▼"
    else:
        arrow = "▲"

    for button in position_buttons:
        if teamA.active:
            if button.get_sensitive():
                button.set_label(arrow)
                button.get_style_context().remove_class("colorB")
                button.get_style_context().add_class("colorA")
        elif teamB.active:
            if button.get_sensitive():
                button.set_label(arrow)
                button.get_style_context().remove_class("colorA")
                button.get_style_context().add_class("colorB")
        else:
            button.set_label("")
            button.get_style_context().remove_class("colorA")
            button.get_style_context().remove_class("colorB")


def change_disks_sensitivity(disk_above, boolean):

    disk_above.button.set_sensitive(boolean)
    for disk in disks:
        if disk != disk_above and disk.x == disk_above.x and disk.dropped:
            disk.button.set_sensitive(not boolean)
            break


def get_clicked_disk(popover):

    button = popover.get_relative_to()
    for disk in disks:
        if disk.button == button:
            return disk


def check_connections(color, x, y):

    # vertical connections
    disks_to_check = []
    for disk in disks:
        if disk.x == x:
            disks_to_check.append(disk)
    if check_victory(color, disks_to_check):
        return True

    # horizontal connections
    disks_to_check = []
    for disk in disks:
        if disk.y == y:
            disks_to_check.append(disk)
    if check_victory(color, disks_to_check):
        return True

    # diagonal connections: linear positive
    disks_to_check = []
    for disk in disks:
        if disk.y - disk.x == (y - x):
            disks_to_check.append(disk)
    if check_victory(color, disks_to_check):
        return True

    # diagonal connections: linear negative
    disks_to_check = []
    for disk in disks:
        if disk.y + disk.x == (y + x):
            disks_to_check.append(disk)
    if check_victory(color, disks_to_check):
        return True

    return False


def check_victory(color, disks_to_check):

    disks_to_win = []
    for disk in disks_to_check:
        if disk.dropped:
            if disk.color == color:
                disks_to_win.append(disk)
                if len(disks_to_win) == 4:
                    return True
            else:
                disks_to_win = []
        else:
            disks_to_win = []

    return False


def show_victory_message(self):

    from victory import Victory

    Victory(self.winGame)

    if teamA.active or teamB.active:
        play_sound('victory.wav')
    reset_game(self, False)


def play_sound(file_name):

    from settings import sound

    if not sound:
        return

    sound_file = os.path.join(SOUNDS_DIR, file_name)
    if os.name == 'posix':
        from simpleaudio.simpleaudio import WaveObject
        WaveObject.from_wave_file(sound_file).play()
    else:
        from winsound import PlaySound, SND_FILENAME, SND_ASYNC
        PlaySound(sound_file, SND_FILENAME | SND_ASYNC)


# initialize main window and manage widgets

class Game(Gtk.Application):

    def __init__(self):

        Gtk.Application.__init__(self, application_id=APP_ID)
        GLib.set_application_name("Quattuor")
        GLib.set_prgname(APP_ID)

        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.path.join(UI_DIR, 'game.ui'))
        self.builder.connect_signals(self)

        for obj in self.builder.get_objects():
            if issubclass(type(obj), Gtk.Buildable):
                name = Gtk.Buildable.get_name(obj)
                setattr(self, name, obj)

        scale_factor = get_scale_factor(self.winGame)
        load_css_provider(scale_factor)


    def do_startup(self):

        Gtk.Application.do_startup(self)

        share_widgets_with_settings(self)
        setup_theme_and_headerbar(self)
        setup_control_layout(self)
        harmonize_size_of_options_window(self)
        build_disks(self)

        get_team_names()
        get_voc_lists()


    def do_activate(self):

        self.add_window(self.winGame)
        self.winGame.set_default_icon_name(APP_ID)
        self.winGame.present()


    # header bar widgets and menus

    def on_btnNewGame_clicked(self, widget):

        reset_game(self, True)
        show_game_options(self)


    def on_menuNew_clicked(self, widget):

        if self.btnNewGame.get_visible():
            reset_game(self, True)
            show_game_options(self)
        else:
            message_text = _("Do you want to abort the current game?")
            dialog = Gtk.MessageDialog(self.winGame, 0, Gtk.MessageType.QUESTION,
                Gtk.ButtonsType.NONE, message_text)
            button_no = dialog.add_button(_("Back"), Gtk.ResponseType.NO)
            button_yes = dialog.add_button(_("Abort"), Gtk.ResponseType.YES)
            button_yes.get_style_context().add_class("destructive-action")
            response = dialog.run()
            if response == Gtk.ResponseType.YES:
                reset_game(self, True)
                show_game_options(self)
            dialog.destroy()


    def on_menuSettings_clicked(self, widget):

        from settings import Settings

        Settings(self.winGame)


    def on_menuEditor_clicked(self, widget):

        from editor import Editor

        Editor(self.winGame)


    def on_menuHelp_clicked(self, widget):

        import webbrowser

        webbrowser.open_new_tab("https://quattuor.readthedocs.io/en/latest/")


    def on_menuAbout_clicked(self, widget):

        from about import About

        About(self.winGame)


    def on_menuQuit_clicked(self, widget):

        if not self.on_winGame_delete_event(self.winGame, 0):
            save_team_names()
            save_voc_lists()
            self.quit()


    # game options widget

    def on_noteOptions_switch_page(self, widget, page, index):

        if index == 0:
            # team mode
            self.boxDuelMode.remove(self.gridVocabularyList)
            self.boxDuelMode.remove(self.gridTeams)
            self.boxClassicMode.remove(self.gridTeams)
            self.boxTeamMode.add(self.gridVocabularyList)
            self.boxTeamMode.add(self.gridTeams)
            self.lblTeamA.set_text(_("Team A"))
            self.lblTeamB.set_text(_("Team B"))
            self.entTeamA.set_placeholder_text(_("Name of first team"))
            self.entTeamB.set_placeholder_text(_("Name of second team"))
            self.lblPlayerNo.show()
            self.cmbPlayerNo.show()
        elif index == 1:
            # duel mode
            self.boxTeamMode.remove(self.gridVocabularyList)
            self.boxTeamMode.remove(self.gridTeams)
            self.boxClassicMode.remove(self.gridTeams)
            self.boxDuelMode.add(self.gridVocabularyList)
            self.boxDuelMode.add(self.gridTeams)
            self.lblTeamA.set_text(_("Player A"))
            self.lblTeamB.set_text(_("Player B"))
            self.entTeamA.set_placeholder_text(_("Name of first player"))
            self.entTeamB.set_placeholder_text(_("Name of second player"))
            self.lblPlayerNo.hide()
            self.cmbPlayerNo.hide()
        else:
            # classic mode
            self.boxTeamMode.remove(self.gridVocabularyList)
            self.boxTeamMode.remove(self.gridTeams)
            self.boxDuelMode.remove(self.gridVocabularyList)
            self.boxDuelMode.remove(self.gridTeams)
            self.boxClassicMode.add(self.gridTeams)
            self.lblTeamA.set_text(_("Player A"))
            self.lblTeamB.set_text(_("Player B"))
            self.entTeamA.set_placeholder_text(_("Name of first player"))
            self.entTeamB.set_placeholder_text(_("Name of second player"))
            self.lblPlayerNo.hide()
            self.cmbPlayerNo.hide()


    def on_btnVocabularyNew_clicked(self, widget):

        from list import show_vocabulary_dialog
        global voc_file_path, voc_list_current

        dialog = show_vocabulary_dialog(self.winGame)

        response = dialog.run()
        if response == Gtk.ResponseType.ACCEPT:
            voc_list_current = {}
            voc_file_path = dialog.get_filename()
            dialog.destroy()
            load_vocabulary_list(self, voc_file_path)
        else:
            dialog.destroy()


    def on_menuRecent_clicked(self, widget):

        global voc_list_current, voc_file_path

        menu_position = int(widget.get_name()[-1])
        voc_list_current = {}
        voc_file_path = voc_lists[menu_position-1]
        load_vocabulary_list(self, voc_file_path)


    def on_btnListSelectAll_clicked(self, widget):

        for i in range(len(self.listUnits)):
            self.listUnits[i][0] = True


    def on_btnListDeselectAll_clicked(self, widget):

        for i in range(len(self.listUnits)):
            self.listUnits[i][0] = False


    def on_btnListTop_clicked(self, widget):

        self.treeUnits.scroll_to_cell(0, None, False)


    def on_btnListUp_clicked(self, widget):

        rect = self.treeUnits.get_visible_rect()
        self.treeUnits.scroll_to_point(0, rect.y - rect.height)


    def on_btnListDown_clicked(self, widget):

        rect = self.treeUnits.get_visible_rect()
        self.treeUnits.scroll_to_point(0, rect.y + rect.height)


    def on_btnListBottom_clicked(self, widget):

        self.treeUnits.scroll_to_cell(len(self.listUnits)-1, None, False)


    def on_treeUnits_row_activated(self, widget, path, column):

        self.listUnits[path][0] = not self.listUnits[path][0]


    def on_entTeam_icon_press(self, widget, position, event):

        widget.set_text("")


    def on_btnOptionsApply_clicked(self, widget):

        if not create_teams(self):
            return

        if self.noteOptions.get_current_page() == 0:
            classic_mode = False
            if not choose_unit(self):
                return
            set_player_number(self, False)
        elif self.noteOptions.get_current_page() == 1:
            classic_mode = False
            if not choose_unit(self):
                return
            set_player_number(self, True)
        else:
            classic_mode = True

        setup_game(self, classic_mode)
        if not Gtk.check_version(3,22,0):
            self.popOptions.popdown()
        else:
            self.popOptions.hide()


    # game control widgets

    def on_btnPlayer_clicked(self, widget):

        global players

        if not len(players):
            for n in range(1, player_number+1):
                players.append(n)
        player = random.randint(0, len(players)-1)
        widget.set_label(_("No. {}").format(str(players[player])))
        players.remove(players[player])

        self.btnVocable.set_sensitive(True)
        self.lblVocable.set_text("?")
        self.btnTranslation.set_sensitive(False)
        self.lblTranslation.set_text("")
        for button in self.gridWinner.get_children():
            button.set_sensitive(False)
            button.set_label("")
        self.btnTeamA.get_style_context().remove_class("colorA")
        self.btnTeamB.get_style_context().remove_class("colorB")
        change_position_buttons_sensitivity(False)
        color_position_buttons(False, False)


    def on_btnVocable_clicked(self, widget):

        from settings import timer_length

        self.btnTranslation.set_sensitive(False)
        self.lblTranslation.set_text("")
        for button in self.gridWinner.get_children():
            button.set_sensitive(False)
            button.set_label("")
        self.btnTeamA.get_style_context().remove_class("colorA")
        self.btnTeamB.get_style_context().remove_class("colorB")
        change_position_buttons_sensitivity(False)
        color_position_buttons(False, False)

        if not self.lblVocable.get_text() in "12345678910":
            self.lblVocable.set_text(str(timer_length))
            GObject.timeout_add(1000, select_vocable, self)


    def on_btnTranslation_clicked(self, widget):

        global translation

        self.lblTranslation.set_text(translation)

        for button in self.gridWinner.get_children():
            button.set_sensitive(True)
        self.btnTeamA.get_style_context().add_class("colorA")
        self.btnTeamB.get_style_context().add_class("colorB")
        self.btnTeamA.set_label("A")
        self.btnTeamB.set_label("B")
        self.btnNoTeam.set_label("X")
        change_position_buttons_sensitivity(False)
        color_position_buttons(False, False)


    def on_btnTeamA_clicked(self, widget):

        change_position_buttons_sensitivity(True)
        color_position_buttons(True, False)


    def on_btnTeamB_clicked(self, widget):

        change_position_buttons_sensitivity(True)
        color_position_buttons(False, True)


    def on_btnNoTeam_clicked(self, widget):

        self.btnPlayer.set_label("?")
        if player_number != 2:
            self.btnVocable.set_sensitive(False)
            self.lblVocable.set_text("")
        else:
            self.lblVocable.set_text("?")
        self.btnTranslation.set_sensitive(False)
        self.lblTranslation.set_text("")
        for button in (widget, self.btnTeamA, self.btnTeamB):
            button.set_label("")
            button.set_sensitive(False)
        self.btnTeamA.get_style_context().remove_class("colorA")
        self.btnTeamB.get_style_context().remove_class("colorB")
        change_position_buttons_sensitivity(False)
        color_position_buttons(False, False)


    # game grid widgetswingame

    def on_btnPosition_clicked(self, widget):

        # drop disk and check for victory
        position = int(widget.get_name()[-1])
        for disk in reversed(disks):
            if disk.x == position:
                if not disk.dropped:
                    disk.dropped = True
                    if self.boxGameControl.get_visible() and self.btnPlayer.get_visible():
                        # allow manipulating of disks only in team mode
                        change_disks_sensitivity(disk, True)
                    if teamA.active:
                        disk.color = teamA.color
                        disk.button.get_style_context().add_class("colorA")
                        if check_connections(teamA.color, disk.x, disk.y):
                            show_victory_message(self)
                            return
                    if teamB.active:
                        disk.color = teamB.color
                        disk.button.get_style_context().add_class("colorB")
                        if check_connections(teamB.color, disk.x, disk.y):
                            show_victory_message(self)
                            return
                    break

        # deactivate the position button when column is full
        if disk.y == 5:
            # the underline property is a hack to make the button permanently insensitive
            widget.set_use_underline(True)
            widget.set_label("")
            widget.set_sensitive(False)
            widget.get_style_context().remove_class("colorA")
            widget.get_style_context().remove_class("colorB")

        # check if game ended in a draw
        buttons_insensitive = 0
        for button in position_buttons:
            if button.get_use_underline():
                buttons_insensitive += 1
        if buttons_insensitive == 7:
            teamA.active = False
            teamB.active = False
            show_victory_message(self)
            return

        play_sound('drop.wav')
        if self.boxGameControl.get_visible():
            self.on_btnNoTeam_clicked(self.btnNoTeam)
        else:
            color_position_buttons(not teamA.active, not teamB.active)


    def on_btnDisk_clicked(self, widget):

        self.popDisk.set_relative_to(widget)
        self.popDisk.set_sensitive(True)

        if not Gtk.check_version(3,22,0):
            self.popDisk.popup()
        else:
            self.popDisk.show_all()


    def on_btnDiskTeamA_clicked(self, widget):

        disk = get_clicked_disk(self.popDisk)
        disk.color = teamA.color
        disk.button.get_style_context().remove_class("colorB")
        disk.button.get_style_context().add_class("colorA")
        if check_connections(teamA.color, disk.x, disk.y):
            teamA.active = True
            teamB.active = False
            show_victory_message(self)

        if not Gtk.check_version(3,22,0):
            self.popDisk.popdown()
        else:
            self.popDisk.hide()


    def on_btnDiskTeamB_clicked(self, widget):

        disk = get_clicked_disk(self.popDisk)
        disk.color = teamB.color
        disk.button.get_style_context().remove_class("colorA")
        disk.button.get_style_context().add_class("colorB")
        if check_connections(teamB.color, disk.x, disk.y):
            teamA.active = False
            teamB.active = True
            show_victory_message(self)

        if not Gtk.check_version(3,22,0):
            self.popDisk.popdown()
        else:
            self.popDisk.hide()


    def on_btnDiskDelete_clicked(self, widget):

        disk = get_clicked_disk(self.popDisk)
        disk.dropped = False
        disk.button.set_sensitive(False)
        disk.button.get_style_context().remove_class("colorA")
        disk.button.get_style_context().remove_class("colorB")
        change_disks_sensitivity(disk, False)

        if not Gtk.check_version(3,22,0):
            self.popDisk.popdown()
        else:
            self.popDisk.hide()


    # quit game

    def on_winGame_delete_event(self, widget, event):

        if self.btnNewGame.get_visible():
            return False
        else:
            message_text = _("Do you want to quit the application?")
            dialog = Gtk.MessageDialog(self.winGame, 0, Gtk.MessageType.QUESTION,
                Gtk.ButtonsType.NONE, message_text)
            button_no = dialog.add_button(_("Back"), Gtk.ResponseType.NO)
            button_yes = dialog.add_button(_("Quit"), Gtk.ResponseType.YES)
            button_yes.get_style_context().add_class("destructive-action")
            response = dialog.run()
            dialog.destroy()
            if response == Gtk.ResponseType.YES:
                return False
            else:
                return True


    def on_winGame_destroy(self, widget):

        save_team_names()
        save_voc_lists()
        self.quit()
