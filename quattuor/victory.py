#!/usr/bin/env python3
#
# This file is part of Quattuor.
#
# Copyright (C) 2020-2021 - Thomas Dähnrich <develop@tdaehnrich.de>
#
# Quattuor is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Quattuor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quattuor. If not, see <http://www.gnu.org/licenses/>.

import gettext
import gi
import os
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gdk
from gi.repository import Gtk

from settings import UI_DIR, _


# main functions of victory

def add_keyboard_accelerators(self):

    accel_group = Gtk.AccelGroup()
    accel_group.connect(Gdk.keyval_from_name('Escape'), 0, 0, self.on_winVictory_delete_event)
    self.winVictory.add_accel_group(accel_group)


# initialize victory window and manage widgets

class Victory(Gtk.Window):

    def __init__(self, transient_window):

        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.path.join(UI_DIR, 'victory.ui'))
        self.builder.connect_signals(self)

        for obj in self.builder.get_objects():
            if issubclass(type(obj), Gtk.Buildable):
                name = Gtk.Buildable.get_name(obj)
                setattr(self, name, obj)

        add_keyboard_accelerators(self)
        self.winVictory.set_transient_for(transient_window)
        self.winVictory.show()


    def on_winVictory_show(self, widget):

        from game import teamA, teamB

        if teamA.active:
            winner_team = teamA.name
            self.lblVictoryTop.get_style_context().remove_class("colorB")
            self.lblVictoryTop.get_style_context().add_class("colorA")
        elif teamB.active:
            winner_team = teamB.name
            self.lblVictoryTop.get_style_context().remove_class("colorA")
            self.lblVictoryTop.get_style_context().add_class("colorB")
        else:
            winner_team = ""

        if winner_team:
            self.headVictory.set_title(_("Congratulations!"))
            self.imgVictory.show()
            self.lblVictoryTop.set_text(winner_team)
            self.lblVictoryBottom.set_text(_("won the game!"))
        else:
            self.headVictory.set_title(_("Game finished!"))
            self.imgVictory.hide()
            self.lblVictoryTop.set_text(_("The game"))
            self.lblVictoryBottom.set_text(_("ended in a draw!"))


    def on_winVictory_delete_event(self, widget, event, *args):

        self.winVictory.hide()
        return True