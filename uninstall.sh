#!/bin/bash
if ! find ./ -path "*uninstall.bak*" | grep -q .
then
    echo "Quattuor is not installed."
    echo "You can install it via './install.sh'."
    exit
fi
sudo ninja -C build uninstall
